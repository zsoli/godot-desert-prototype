extends AnimationPlayer


export var cutscene_camera_node: NodePath
var cutscene_camera: Camera


# Called when the node enters the scene tree for the first time.
func _ready():
	cutscene_camera = get_node(cutscene_camera_node)
	var ret: int = self.connect("animation_finished", self, "_animation_finished")
	assert(ret == OK)


func _animation_finished(var anim: String) -> void:
	print_debug("asd")
	if anim == "StartCutscene":
		cutscene_camera.current = false
