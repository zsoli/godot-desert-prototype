class_name DesertController
extends Node

export(float, 0, 2) var storm_factor: float = 0.5 setget set_storm_factor
export(float, 0, 360) var wind_direction: float = 0.0 setget set_wind_direction
export(int, 0, 4096) var max_dust_particles: int = 32
export(int, 0, 4096) var max_specular_particles: int = 32

var ready: bool = false

# desert pass shader
export var pass_material_node: String
var pass_material: ShaderMaterial

# terrain
export var terrain_mesh_node: NodePath
var terrain_mesh: MeshInstance
var terrain_material: ShaderMaterial

# dust
export var dust_particle_node: NodePath
var dust_particle: Particles
var dust_particle_material: ParticlesMaterial

# dust
export var specular_particle_node: NodePath
var specular_particle: Particles
var specular_particle_material: ParticlesMaterial

# terrain shader initial values
var wind_speed: float
var wind_detail_speed: float
var wind_strength: float

# dust particles initial values
var dust_initial_velocity: float

# specular particles initial values
var specular_initial_velocity: float


var t: float
var wind_offset: float


func update_terrain_shader():
	if !ready: return
	
	terrain_material.set_shader_param("wind_strength", wind_strength * storm_factor)
	terrain_material.set_shader_param("wind_speed", wind_speed * storm_factor)
	terrain_material.set_shader_param("wind_detail_speed", wind_detail_speed * storm_factor)
	terrain_material.set_shader_param("wind_direction", wind_direction)
	terrain_material.set_shader_param("wind_offset", wind_offset)


func update_particle_systems():
	if !ready: return

	var dust_particle_c: int = int(max_dust_particles * storm_factor)
	var specular_particle_c: int = int(max_specular_particles * storm_factor)

	dust_particle.emitting = dust_particle_c > 0
	dust_particle.amount = dust_particle_c + 1
	dust_particle_material.initial_velocity = dust_initial_velocity * storm_factor

	specular_particle.emitting = specular_particle_c > 0
	specular_particle.amount = specular_particle_c + 1
	specular_particle_material.initial_velocity = specular_initial_velocity * storm_factor


func update_all():
	pass
	# update_terrain_shader()
	# update_particle_systems()


func set_storm_factor(v: float) -> void:
	storm_factor = v
	update_all()


func set_wind_direction(d: float) -> void:
	wind_direction = d;
	# update_terrain_shader()


func _ready():
	terrain_mesh = get_node(terrain_mesh_node)
	assert(terrain_mesh != null, "terrain_mesh_node is not set!")

	terrain_material = terrain_mesh.get_active_material(0)
	assert(terrain_material != null, "could not get the terrain mesh's material!")

	dust_particle = get_node(dust_particle_node)
	assert(dust_particle != null, "dust_particle_node is not set!")
	
	dust_particle_material = dust_particle.get_process_material()
	assert(dust_particle_material != null, "could not get dust particle material!")

	specular_particle = get_node(specular_particle_node)
	assert(specular_particle != null, "specular_particle_node is not set!")
	
	specular_particle_material = specular_particle.get_process_material()
	assert(specular_particle_material != null, "could not get specular particle material!")

	pass_material = load(pass_material_node)
	assert(pass_material != null, "failed to load pass material!")

	store_initial_values()
	ready = true
	update_all()


func store_initial_values():
	wind_speed = terrain_material.get_shader_param("wind_speed")
	wind_detail_speed = terrain_material.get_shader_param("wind_detail_speed")
	wind_strength = terrain_material.get_shader_param("wind_strength")
	dust_initial_velocity = dust_particle_material.initial_velocity
	specular_initial_velocity = specular_particle_material.initial_velocity


func _process(delta):
	# t += delta / 5
	wind_offset += delta * wind_speed * storm_factor
	update_terrain_shader()
	# set_storm_factor(0.5 + cos(t) / 2)
