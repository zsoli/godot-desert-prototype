extends ItemHolder
class_name ContraptionPillar

var t: float = 0.0
# var rot_t: float

var item_holder_y: float
var shift: float


func _ready():
	# randomize()
	shift = randf() * 10
	item_holder_y = item_holder.translation.y


func _process(delta):
	t += delta
	# rot_t += delta * 100

	var freq = 2.0
	var ampl = 0.25
	var target_pos = item_holder_y + sin((t + shift) * freq) * ampl

	# rot_t = wrapf(rot_t + 0.1, 0, 360)

	item_holder.translation.y = lerp(item_holder.translation.y, target_pos, 0.1)
	item_holder.rotation_degrees.y = lerp(item_holder.rotation_degrees.y, t * 100, 0.5)
