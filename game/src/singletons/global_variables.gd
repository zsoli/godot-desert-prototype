extends Node

var debug_mode: bool = true
var settings_manager := SettingsManager.new()
var scene_switcher := SceneSwitcher.new()

# To fix a bug (in engine?) which causes the native scripts to not load
# making the terrain rendering and collision messed up.
# (see https://github.com/Zylann/godot_heightmap_plugin/issues/236 for more info)
# The fix is to load the utils before spawning a new thread to load and instantiate the
# new scene. This variable should stay alive until the game is running.
var _image_utils = preload("res://addons/zylann.hterrain/native/factory.gd").get_image_utils()