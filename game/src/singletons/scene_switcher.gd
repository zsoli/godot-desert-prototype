class_name SceneSwitcher
extends Reference

var loaders: Dictionary
class LoadObj:
	var thread: Thread
	var old_scene: Node
	var new_scene
	var progress: int = 0 setget _set_progress

	signal loading(progress)

	func _init(var _old_scene: Node) -> void:
		self.thread = Thread.new()
		self.old_scene = _old_scene


	func _set_progress(var value: int) -> void:
		progress = value
		emit_signal("loading", self.progress)


	func start_loading(var scene: String, var prio := Thread.PRIORITY_LOW) -> LoadObj:
		var ret: int = self.thread.start(self, "_load_scene", scene, prio)
		assert(ret == OK)
		print_debug("SceneSwitcher: Started loading thread: '%s'" % self.thread)

		return self
	
	
	# load resource on another thread using ResourceInteractiveLoader
	func _load_scene(scene: String) -> void:
		var loader: ResourceInteractiveLoader = null
		var err: int = OK

		print_debug("SceneSwitcher: Start of loading '%s'" % scene)
		OS.delay_msec(1000)

		# get loader
		loader = ResourceLoader.load_interactive(scene)
		if loader == null:
			push_error("SceneSwitcher: Failed to get interactive loader")
			return

		# actually load the resource
		while (err == OK):
			err = loader.poll()
			# OS.delay_msec(500)
			self.progress = int(loader.get_stage() * 100.0 / loader.get_stage_count())

		if err == ERR_FILE_EOF:
			self.new_scene = loader.get_resource()
			self.progress = 100
		else:	
			push_error("SceneSwitcher: Error during loading")

		print_debug("SceneSwitcher: Loaded '%s'" % self.new_scene)


func load_scene(var old_scene: Node, var new_scene: String) -> LoadObj:
	if loaders.has(new_scene):
		return null
	
	var loader = LoadObj.new(old_scene).start_loading(new_scene)
	loaders[new_scene] = loader
	return loader


func get_progress(var scene) -> int:
	if !loaders.has(scene): 
		return -1
	
	return loaders[scene].progress


func goto_scene(path) -> void:
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.

	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:

	call_deferred("_deferred_goto_scene", path)


func _deferred_goto_scene(path) -> void:
	var loader: LoadObj = loaders[path]
	assert(loader != null)

	# wait for loading thread to finish in case we got here before loading finished
	loader.thread.wait_to_finish()
	
	# replace old scene with new one
	var parent_scene: Node = loader.old_scene.get_parent()
	assert(parent_scene != null)

	loader.old_scene.free()
	parent_scene.add_child(loader.new_scene.instance())

	# delete loader
	if (loaders.erase(path)):
		print_debug("SceneSwitcher: Removed loader '%s'" % path)
