class_name SettingsManager
extends ConfigFile

enum {
    VSYNC = 0,
    FXAA,
    AUDIO_OUTPUT_MASTER_VOL,
    AUDIO_OUTPUT_GAMEPLAY_VOL,
    AUDIO_OUTPUT_MUSIC_VOL,
    RESOLUTION_WIDTH,
    RESOLUTION_HEIGHT,
    MSAA_LEVEL,
    SHARPENING,
    AUTO_EXPOSURE,
    FPS_LIMIT
}

const Section: Dictionary = {
    GRAPHICS = "graphics",
    ENGINE = "engine",
    SOUND = "sound"
}

var options: Dictionary = {
    VSYNC:                      SettingID.new(Section.GRAPHICS, "vsync", true),
    FXAA:                       SettingID.new(Section.GRAPHICS, "fxaa", false),
    MSAA_LEVEL:                 SettingID.new(Section.GRAPHICS, "msaa_level", 0),
    SHARPENING:                 SettingID.new(Section.GRAPHICS, "sharpening", 0),
    AUTO_EXPOSURE:              SettingID.new(Section.GRAPHICS, "auto_exposure", false),
    RESOLUTION_WIDTH:           SettingID.new(Section.GRAPHICS, "resolution_width", 1920),
    RESOLUTION_HEIGHT:          SettingID.new(Section.GRAPHICS, "resolution_height", 1080),
    FPS_LIMIT:                  SettingID.new(Section.ENGINE, "fps_limit", 0),
    AUDIO_OUTPUT_MASTER_VOL:    SettingID.new(Section.SOUND, "master_volume", 0),
    AUDIO_OUTPUT_GAMEPLAY_VOL:  SettingID.new(Section.SOUND, "gameplay_volume", 0),
    AUDIO_OUTPUT_MUSIC_VOL:     SettingID.new(Section.SOUND, "music_volume", 0)
}

const SETTINGS_FILE: String = "settings.cfg"
const GODOT_FILE_PATH: String = "user://" + SETTINGS_FILE
var absolute_file_path: String
var changed: bool


class SettingID:
    var section: String
    var key: String
    var default_value

    func _init(s: String, k: String, dv = null) -> void:
        self.section = s
        self.key = k
        self.default_value = dv


func _init() -> void:
    absolute_file_path = OS.get_user_data_dir() + "/" + SETTINGS_FILE

    var ret: int = load_settings()
    assert(ret == OK)

    _print_debug("Initialized")


func save_settings() -> void:
    if (!changed):
        return
    
    changed = false

    var ret: int = save(GODOT_FILE_PATH)
    assert(ret == OK)

    _print_debug("Saved settings file to: '%s'" % absolute_file_path)


func load_settings() -> int:
    self.clear()
    
    var ret: int = self.load(GODOT_FILE_PATH)
    match (ret):
        ERR_FILE_NOT_FOUND:
            _print_debug("Settings file not found on disk")
            ret = OK
        OK:
            _print_debug("Loaded settings file: '%s'" % absolute_file_path)
        _:
            _push_error("Falied to load settings file, error code: %d" % ret)
    
    return ret


func set_option(option: int, value) -> void:
    var id: SettingID = _get_setting_id(option)

    if (get_value(id.section, id.key, id.default_value) != value):
        changed = true
        
    set_value(id.section, id.key, value)


func get_option(option: int) -> String:
    var id: SettingID = _get_setting_id(option)
    return get_value(id.section, id.key, id.default_value)


func _get_setting_id(option: int) -> SettingID:
    _assert(option in options, "No option is stored for ID: %d" % option)
    return options[option]


func _print_debug(msg: String) -> void:
    print_debug("SettingsManager: %s" % msg)


func _push_error(msg: String) -> void:
    push_error("SettingsManager: %s" % msg)


func _assert(expr: bool, msg: String):
    assert(expr, "SettingsManager: %s" % msg)