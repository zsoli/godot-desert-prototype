extends KinematicBody
class_name Player

# nodes
var head: Spatial
var camera: Camera
var raycast: RayCast
var item_holder: Spatial

export var head_node: NodePath
export var camera_node: NodePath
export var raycast_node: NodePath
export var item_holder_node: NodePath

export var asp_footsteps_node: NodePath
var asp_footsteps: Array
var footsteps_location: Spatial

export var hud_node: NodePath
var hud: Node

## jump multiplier
export var jump_height_multiplier: float = 20.0

## jump multiplier
export var sprint_multiplier: float = 2.0

## gravity strength
export var gravity_multiplier: float = 50.0 

## velocity deacceleration when jumping
export var jump_deacceleration: float = 0.02

## velocity deacceleration when on floor
export var floor_deacceleration: float = 0.25

## velocity acceleration when on floor
export var floor_acceleration: float = 10.0

## mouse sensitivity multiplier
export var mouse_acceleration_multiplier: float = 1.0

## how much can we look up and down in degrees
export var LOOK_UP_DOWN_LIMIT: float = 89.0

## smoothness of item pickup animation
export var ITEM_POS_SMOOTHNESS: float = 0.25

## smoothness of item pickup animation
export var MAX_SLOPE_ANGLE: float = 30.0

const GRAVITY_DIRECTION := Vector3.DOWN
const MOUSE_SENSITIVITY_BASE: float = 0.025 * -1.0
const EPSILON = 0.001 # error threshold

var velocity := Vector3.ZERO
var target_velocity := Vector3.ZERO
var item_highlighted: Interactable
var item_picked_up: Pickupable
var movement_accel: Vector3
var jump_accel: Vector3
var gravity: Vector3
var mouse_accel: float
var slope_angle: float
var is_on_floor: int

var camera_y_pos: float
var target_camera_fov: float = 60.0

var audio_player: PlayerAudioManager

# timers
var camera_blob_t: float = 0.0
var audio_t: float = 0.0
var fall_t: float = 0.0
var t: float = 0.0
var on_floor_t: float
var jump_t: float
var footsteps_t: float


# keyboard events handler
func proc_input() -> void:
	var event := Input

	var z1: float = event.is_action_pressed("move_backward")
	var z2: float = event.is_action_pressed("move_forward")
	var x1: float = event.is_action_pressed("move_right")
	var x2: float = event.is_action_pressed("move_left")
	var y: float = event.is_action_just_pressed("jump")
	var sprint: float = event.is_action_pressed("sprint")
	# var crouch: float = event.is_action_just_pressed("crouch")

	# movement along x and z axis
	target_velocity = (z1 - z2) * transform.basis.z + (x1 - x2) * transform.basis.x
	target_velocity = target_velocity.normalized()
	
	# apply speed modifier
	target_velocity *= movement_accel * (1 + (sprint_multiplier - 1) * sprint)

	# movement along y axis (jump)
	target_velocity += jump_accel * y * is_on_floor * int(jump_t > 0.5)

	# reset the jump timer if pressed jump and in air
	jump_t *= 1 - (y * is_on_floor * int(jump_t > 0.5))


func proc_movement(delta: float) -> void:
	var fa: float = get_floor_angle()
	var d: float = lerp(jump_deacceleration, floor_deacceleration, is_on_floor)
	
	# apply gravity only if slope angle higher than specified value
	target_velocity += gravity * delta * int(fa > slope_angle + EPSILON)

	velocity.x = lerp(velocity.x, target_velocity.x, d)
	velocity.y += target_velocity.y
	velocity.z = lerp(velocity.z, target_velocity.z, d)

	velocity = move_and_slide(velocity, Vector3.UP, true)


func proc_camera(delta: float) -> void:
	var event := Input
	var z1: float = event.is_action_just_released("zoom_in")
	var z2: float = event.is_action_just_released("zoom_out")

	target_camera_fov += (z1 - z2) * 10
	target_camera_fov = clamp(target_camera_fov, 10, 120)
	camera.fov = lerp(camera.fov, target_camera_fov, 0.2)
	
	var v_len: float = Vector2(target_velocity.x, target_velocity.z).abs().length()
	
	var rot_ampl: float = 0.04 * v_len
	var rot_freq: float = 0.5 * v_len

	var mov_ampl: float = 0.01 * v_len
	var mov_freq: float = 1.0 * v_len
	
	var moving: int = v_len > 0 + EPSILON

	camera_blob_t += delta * moving
	t += delta
	t *= moving

	var camera_z_rot = sin(camera_blob_t * rot_freq) * rot_ampl * is_on_floor
	camera_y_pos = cos(camera_blob_t * mov_freq) * mov_ampl * is_on_floor

	if camera_y_pos < -0.05 && audio_t > 0.2:
		audio_player.play_footstep()
		audio_t = 0

	if is_on_floor:
		camera_y_pos -= fall_t * delta * 250 * 2
		fall_t = 0

	var rot_w: float = clamp(t * delta * 100, 0.01, 0.2)
	var pos_w: float = clamp(t * delta * 100, 0.1, 0.2)

	camera.rotation_degrees.z = lerp(camera.rotation_degrees.z, camera_z_rot, rot_w)
	camera.translation.y = lerp(camera.translation.y, camera_y_pos, pos_w)


# mouse events handler
func handle_mouse_input(event) -> void:
	if !(event is InputEventMouseMotion) || Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return
	
	var rel = event.relative * mouse_accel
	head.rotate_x(deg2rad(rel.y))
	rotate_y(deg2rad(rel.x))
	head.rotation_degrees.x = clamp(head.rotation_degrees.x, -LOOK_UP_DOWN_LIMIT, LOOK_UP_DOWN_LIMIT)


# func play_footsteps() -> void:
# 	if velocity.length() > 1 && footsteps_t > 0:
# 		# asp_footsteps[randi() % asp_footsteps.size()].play()
# 		footsteps_t = 0


# picking up items
func handle_interact(event) -> void:
	if !event.is_action_pressed("interact") || item_highlighted == null:
		return

	if item_highlighted is ItemHolder && item_highlighted.item == null:
		if item_picked_up != null:
			item_picked_up.interact()
		item_highlighted.interact(item_picked_up)
		item_picked_up = null
	
	if item_highlighted is Pickupable && item_picked_up == null:
		item_highlighted.interact(item_picked_up)
		item_picked_up = item_highlighted


# select pickupable items when looking at it
func check_raycast() -> void:
	item_highlighted = raycast.get_collider() as Interactable

	if item_highlighted == null:
		# hud.hide_interact()
		return
	
	item_highlighted.highlight()
	# hud.show_interact()


func _input(event) -> void:
	handle_mouse_input(event)
	handle_interact(event)


func _process(delta) -> void:
	proc_camera(delta)

	jump_t += delta
	audio_t += delta
	footsteps_t += delta
	fall_t += delta * int(velocity.y < 0 - EPSILON)
	fall_t *= int(velocity.y < 0 + EPSILON)

	if item_picked_up != null:
		item_picked_up.global_transform = item_picked_up.global_transform.interpolate_with(item_holder.global_transform, ITEM_POS_SMOOTHNESS)


func _physics_process(delta) -> void:
	on_floor_t += delta

	if (is_on_floor && on_floor_t > 0.25) || (!is_on_floor && on_floor_t > EPSILON):
		on_floor_t = 0
		is_on_floor = int(is_on_floor())

	check_raycast()
	proc_input()
	proc_movement(delta)
	

func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	# Engine.time_scale = 0.2

	head = get_node(head_node)
	assert(head != null)

	camera = get_node(camera_node)
	assert(camera != null)

	raycast = get_node(raycast_node)
	assert(raycast != null)

	item_holder = get_node(item_holder_node)
	assert(item_holder != null)

	# hud = get_node(hud_node)
	# assert(hud != null)

	audio_player = PlayerAudioManager.new()
	add_child(audio_player)
	audio_player.init()

	movement_accel = Vector3.ONE * floor_acceleration
	jump_accel = Vector3.UP * jump_height_multiplier
	gravity = GRAVITY_DIRECTION * gravity_multiplier
	mouse_accel = MOUSE_SENSITIVITY_BASE * mouse_acceleration_multiplier
	slope_angle = deg2rad(MAX_SLOPE_ANGLE)
