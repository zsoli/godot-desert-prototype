class_name PlayerAudioManager
extends Node

var queue: Array
var available: Array
var sampler_count: int = 3


func init() -> void:
    var i: int = 0

    while i < sampler_count:
        var s := AudioStreamPlayer.new()
        s.stream = load("res://res/audio/footstep_sand_var" + str(randi() % 3 + 1) + ".ogg")
        s.bus = "Footsteps"
        # s.volume_db = -20

        add_child(s)
        available.append(s)
        
        var ret := s.connect("finished", self, "_on_sampler_finished", [s])
        assert(ret == OK)

        i += 1


func play_footstep() -> void:
    # samplers[randi() % sampler_count].play()

    if available.size() <= 0:
        return

    var s = randi() % available.size()

    available[s].play()
    available.remove(s)
    # print_debug("played")


func _on_sampler_finished(s) -> void:
    available.append(s)
    # print_debug("sampler '%s' finished" % s)