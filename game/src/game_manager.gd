class_name GameManager
extends Node

# contraption
export var contraption_node: NodePath
var contraption: ContraptionTerminal

# desert controller
export var desert_controller_node: NodePath
var desert_controller: DesertController

# magic items
export var magic_item_nodes: Array
var magic_items: Array

# player
export var player_node: NodePath
var player: Player

var t: float
var current_closest_max: float
var current_dst: float
var item_count: int
var base_factor: float


func _ready() -> void:
	desert_controller = get_node(desert_controller_node)
	assert(desert_controller != null, "failed to get desert controller!")

	player = get_node(player_node)
	assert(player != null, "failed to get player node")

	contraption = get_node(contraption_node)
	assert(contraption != null)

	for p in contraption.pillars:
		var ret: int = p.connect("item_added", self, "_on_pillar_activated")
		assert(ret == OK, "ContraptionTerminal: failed to connect to item_added signal for pillar")

	for path in magic_item_nodes:
		var item = get_node(path)
		assert(item != null, "failed to get %s" % path)

		magic_items.append(item)

	item_count = magic_items.size()

	current_closest_max = dst_to_closest_item()
	current_dst = current_closest_max


func _on_pillar_activated(pillar) -> void:
	for i in magic_items.size():
		var item = magic_items[i]
		var dst = dst_to(item, pillar.item)

		# print_debug("dst between %s and %s is %f" % [item, pillar.item, dst])

		if dst < 1.0:
			# print_debug("removed %s" % item)
			magic_items.remove(i)
			current_closest_max = dst_to_closest_item()
			current_dst = current_closest_max
			base_factor += 1.0 / item_count
			break


func _process(delta: float) -> void:
	t += delta

	if magic_items.size() == 0 || t < 0.1:
		return
	
	t = 0
	var m = dst_to_closest_item()
	# print_debug("m = %f" % m)

	if m < current_dst:
		current_dst = m

		var ratio: float = 1.0 - current_dst / current_closest_max
		ratio *= 1.0 / item_count
		ratio += base_factor

		desert_controller.set_storm_factor(ratio)


func dst_to(obj1, obj2) -> float:
	var a := Vector3(obj1.get_global_transform().origin - obj2.get_global_transform().origin)
	return sqrt((a.x * a.x) + (a.y * a.y))


func dst_to_closest_item() -> float:
	var d: float = 99999.0

	for i in magic_items:
		var b = dst_to(i, player)

		if b < d:
			d = b

	return d
