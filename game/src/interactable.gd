class_name Interactable
extends StaticBody

export var mesh_node: NodePath

var parent: Interactable = null
var material: Material = null
var highlight_t: float = 0.0

export var highlight_color: Color = Color.red
export var highlight_speed: float = 2.5


func highlight():
	highlight_t = 1.0