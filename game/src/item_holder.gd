extends Interactable
class_name ItemHolder

signal item_added

export var linterp_speed = 0.5
export var item_holder_node: NodePath

var item_holder
var item: Interactable = null


func interact(entity = null):
	.interact(entity)
	
	if entity != null:
		item = entity
		item.parent = self
		print_debug("used item holder %s" % self)
		emit_signal("item_added", self)


func _process(_delta):
	# ._process(delta)

	if item != null:
		item.global_transform = item.global_transform.interpolate_with(item_holder.global_transform, linterp_speed)


func _ready():
	item_holder = get_node(item_holder_node)
	assert(item_holder != null, "ItemHolder needs item position node set!")
