extends StaticBody
class_name ContraptionTerminal

export var pillar_nodes: Array
var pillars: Array
var pillars_activated: int = 0

# particles
export var particle_node: NodePath
var particle: Particles

# export var scene_switcher_collision_node: NodePath
# var scene_switcher_collision: CollisionShape


func _ready():
	particle = get_node(particle_node)
	assert(particle != null, "failed to get particle system")

	# scene_switcher_collision = get_node(scene_switcher_collision_node)
	# assert(scene_switcher_collision != null)
	
	particle.set_emitting(false)
	# scene_switcher_collision.disabled = true

	pillars = Array()
	for pn in pillar_nodes:
		var p: ContraptionPillar = get_node(pn)
		assert(p != null)

		var ret: int = p.connect("item_added", self, "_on_pillar_activated")
		if ret != OK:
			printerr("ContraptionTerminal: failed to connect to item_added signal for pillar %" % p)

		pillars.append(p)

	for p in pillars:
		print_debug("ContraptionTerminal: attached pillar: %s" % p)


func _on_pillar_activated(pillar):
	print_debug("ContraptionTerminal: item_added signal recv from pillar %s" % pillar)

	pillars_activated += 1
	if pillars_activated >= pillars.size():
		activate_contraption()


func activate_contraption():
	print_debug("ContraptionTerminal: activated")
	particle.set_emitting(true)
	# scene_switcher_collision.disabled = false
