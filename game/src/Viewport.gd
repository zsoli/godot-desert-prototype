class_name CustomViewport
extends Viewport

var target_size: Vector2


func _ready():
	var sm: SettingsManager = GlobalVariables.settings_manager
	var res_width: int = int(sm.get_option(sm.RESOLUTION_WIDTH))
	var res_height: int = int(sm.get_option(sm.RESOLUTION_HEIGHT))
	
	target_size = Vector2(res_width, res_height)

	# connect to root viewport
	var ret: int = get_viewport().connect("size_changed", self, "_on_root_viewport_size_changed")
	assert(ret == OK)
	
	self._on_root_viewport_size_changed()


func _on_root_viewport_size_changed() -> void:
	set_size(target_size)


func set_size(var size: Vector2) -> void:
	target_size = size
	.set_size(size)
	print_debug("updated viewport to size %sx%s" % [size.x, size.y])
