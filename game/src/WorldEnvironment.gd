extends WorldEnvironment

export var desert_area_node: NodePath
var desert_area: Area

export var player_node: NodePath
var player: Player


func _ready():
	desert_area = get_node(desert_area_node)
	assert(desert_area != null, "Desert Area node is not set!")
	
	player = get_node(player_node)
	assert(player != null, "player node is not set!")
	
	var ret = desert_area.connect("body_entered", self, "_on_DesertArea_area_entered")
	assert(ret == OK, "failed to connect area_entered signal!")
	
	ret = desert_area.connect("body_exited", self, "_on_DesertArea_area_exited")
	assert(ret == OK, "failed to connect area_exited signal!")


func _on_DesertArea_area_entered(body):
	print_debug("%s entered the desert area" % body)
	if body != player:
		return
		
	self.environment.fog_depth_begin = 5
	self.environment.fog_depth_end = 100
	print_debug("changed fog")


func _on_DesertArea_area_exited(body):
	print_debug("%s exited the desert area" % body)
	if body != player:
		return
		
	self.environment.fog_depth_begin = 500
	self.environment.fog_depth_end = 1000
	print_debug("changed fog")
