extends Interactable
class_name Pickupable

const COLLISION_LAYER: int = 1
var picked_up: bool = false

func interact(_entity = null):
	.interact(_entity)

	picked_up = !picked_up

	if picked_up:
		print_debug("picked up item %s" % self)
		set_collision_layer_bit(COLLISION_LAYER, false)
	else:
		print_debug("dropped item %s" % self)
		set_collision_layer_bit(COLLISION_LAYER, true)

	if parent != null:
		parent.item = null
