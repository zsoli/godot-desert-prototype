extends Control

export var ssao_switch_node: NodePath
export var ssao_reflections_switch_node: NodePath
export var debug_mode_switch_node: NodePath

export var auto_exposure_switch_node: NodePath
var auto_exposure_switch: Control

export var sharpen_slider_node: NodePath
var sharpen_slider: Control

export var fxaa_switch_node: NodePath
var fxaa_switch: Control

export var vsync_switch_node: NodePath
var vsync_switch: Control

export var msaa_options_node: NodePath
var msaa_options: Control

export var animation_player_node: NodePath
var animation_player: AnimationPlayer

export var master_audio_node: NodePath
var master_audio: Control

export var gameplay_audio_node: NodePath
var gameplay_audio: Control

export var music_audio_node: NodePath
var music_audio: Control

export var fps_limiter_node: NodePath
var fps_limiter: Control

export var viewport_node: NodePath
var viewport: Node

export var resolution_width_node: NodePath
var resolution_width: Control
export var resolution_height_node: NodePath
var resolution_height: Control

class BusIDX:
	var master: int
	var gameplay: int
	var music: int

	func _init():
		self.master = AudioServer.get_bus_index("Master")
		self.gameplay = AudioServer.get_bus_index("Gameplay")
		self.music = AudioServer.get_bus_index("Music")

var mouse_pos: Vector2
var camera: Camera
var menu_open: bool = false
var bus_idx := BusIDX.new()
var settings_manager: SettingsManager


func _ready() -> void:
	# various nodes and signals
	animation_player = get_node(animation_player_node)
	var ssao_switch := get_node(ssao_switch_node)
	var ssao_reflections_switch := get_node(ssao_reflections_switch_node)
	var debug_mode_switch := get_node(debug_mode_switch_node)
	self._connect(animation_player, "animation_finished", "_animation_finished")
	self._connect(ssao_switch, "toggled", "_ssao_switch_toggled")
	self._connect(ssao_reflections_switch, "toggled", "_ssao_reflections_switch_toggled")
	self._connect(debug_mode_switch, "toggled", "_debug_mode_switch_toggled")

	# fps limiter
	fps_limiter = get_node(fps_limiter_node)
	self._connect(fps_limiter, "value_changed", "_fps_limit_value_changed")

	# auto exposure
	auto_exposure_switch = get_node(auto_exposure_switch_node)
	self._connect(auto_exposure_switch, "toggled", "_auto_exposure_switch_toggled")

	# sharpening
	sharpen_slider = get_node(sharpen_slider_node)
	self._connect(sharpen_slider, "value_changed", "_sharpen_value_changed")

	# fxaa
	fxaa_switch = get_node(fxaa_switch_node)
	self._connect(fxaa_switch, "toggled", "_fxaa_switch_toggled")

	# vsync switch
	vsync_switch = get_node(vsync_switch_node)
	self._connect(vsync_switch, "toggled", "_vsync_switch_toggled")
	
	# master audio
	master_audio = get_node(master_audio_node)
	self._connect(master_audio, "value_changed", "_audio_value_changed", [bus_idx.master])

	# gameplay audio
	gameplay_audio = get_node(gameplay_audio_node)
	self._connect(gameplay_audio, "value_changed", "_audio_value_changed", [bus_idx.gameplay])

	#  music audio
	music_audio = get_node(music_audio_node)
	self._connect(music_audio, "value_changed", "_audio_value_changed", [bus_idx.music])

	# msaa
	msaa_options = get_node(msaa_options_node)
	self._connect(msaa_options, "item_selected", "_msaa_item_selected")

	# viewport and camera
	viewport = get_node(viewport_node)
	camera = viewport.get_camera()

	# resolution width and height
	resolution_width = get_node(resolution_width_node)
	self._connect(resolution_width, "value_changed", "_resolution_width_value_changed")
	resolution_height = get_node(resolution_height_node)
	self._connect(resolution_height, "value_changed", "_resolution_height_value_changed")

	# others
	self.visible = false
	mouse_pos = viewport.size / 2.0

	settings_manager = GlobalVariables.settings_manager
	self._set_settings(settings_manager)


func _connect(var node: Node, var sgnl: String, var fn: String, var binds: Array = []) -> void:
	var ret: int = node.connect(sgnl, self, fn, binds)
	assert(ret == OK)


func _unhandled_input(event) -> void:
	if !event.is_action_pressed("ui_cancel"):
		return
		
	menu_open = !menu_open
	
	# order is important when restoring mouse and storing mouse positions
	if menu_open:
		self._on_menu_open()
	else:
		self._on_menu_close()


func _on_menu_open() -> void:
	get_tree().paused = true
	self.visible = true
	animation_player.play("Open")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	viewport.warp_mouse(mouse_pos)


func _on_menu_close() -> void:
	get_tree().paused = false
	animation_player.play("Close")
	mouse_pos = viewport.get_mouse_position()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	settings_manager.save_settings()


func _set_settings(var s: SettingsManager) -> void:
	vsync_switch.set_pressed(s.get_option(s.VSYNC))
	fxaa_switch.set_pressed(s.get_option(s.FXAA))
	sharpen_slider.set_value(s.get_option(s.SHARPENING))
	msaa_options.select(int(s.get_option(s.MSAA_LEVEL)) / 2.0)
	auto_exposure_switch.set_pressed(s.get_option(s.AUTO_EXPOSURE))
	fps_limiter.set_value(s.get_option(s.FPS_LIMIT))

	resolution_width.set_value(s.get_option(s.RESOLUTION_WIDTH))
	resolution_height.set_value(s.get_option(s.RESOLUTION_HEIGHT))

	master_audio.set_value(s.get_option(s.AUDIO_OUTPUT_MASTER_VOL))
	gameplay_audio.set_value(s.get_option(s.AUDIO_OUTPUT_GAMEPLAY_VOL))
	music_audio.set_value(s.get_option(s.AUDIO_OUTPUT_MUSIC_VOL))


func _update_viewport_size() -> void:
	var width: int = int(settings_manager.get_option(settings_manager.RESOLUTION_WIDTH))
	var height: int = int(settings_manager.get_option(settings_manager.RESOLUTION_HEIGHT))
	var size := Vector2(width, height)
	viewport.set_size(size)


# signal handlers
func _resolution_width_value_changed(var val: float) -> void:
	settings_manager.set_option(SettingsManager.RESOLUTION_WIDTH, int(val))
	self._update_viewport_size()

	
func _resolution_height_value_changed(var val: float) -> void:
	settings_manager.set_option(SettingsManager.RESOLUTION_HEIGHT, int(val))
	self._update_viewport_size()


func _audio_value_changed(var val: float, var bus_id: int) -> void:
	var audio_type: int

	match AudioServer.get_bus_name(bus_id).to_lower():
		"master":
			audio_type = SettingsManager.AUDIO_OUTPUT_MASTER_VOL
		"gameplay":
			audio_type = SettingsManager.AUDIO_OUTPUT_GAMEPLAY_VOL
		"music":
			audio_type = SettingsManager.AUDIO_OUTPUT_MUSIC_VOL
		_:
			push_error("failed to get audio bus name with id: %d" % bus_id)

	settings_manager.set_option(audio_type, val)
	AudioServer.set_bus_volume_db(bus_id, val)
	

func _fps_limit_value_changed(var val: float) -> void:
	var v: int = int(val)
	settings_manager.set_option(SettingsManager.FPS_LIMIT, v)
	Engine.set_target_fps(v)


func _animation_finished(var anim: String) -> void:
	if anim == "Close":
		self.visible = false


func _ssao_switch_toggled(var pressed: bool) -> void:
	camera.environment.ssao_enabled = pressed


func _ssao_reflections_switch_toggled(var pressed: bool) -> void:
	camera.environment.ss_reflections_enabled = pressed


func _debug_mode_switch_toggled(var pressed: bool) -> void:
	GlobalVariables.debug_mode = pressed

	
func _auto_exposure_switch_toggled(var pressed: bool) -> void:
	settings_manager.set_option(SettingsManager.AUTO_EXPOSURE, pressed)
	camera.environment.auto_exposure_enabled = pressed


func _vsync_switch_toggled(var pressed: bool) -> void:
	settings_manager.set_option(SettingsManager.VSYNC, pressed)
	OS.set_use_vsync(pressed)

	
func _fxaa_switch_toggled(var pressed: bool) -> void:
	settings_manager.set_option(SettingsManager.FXAA, pressed)
	viewport.set_use_fxaa(pressed)


func _sharpen_value_changed(var val: float) -> void:
	settings_manager.set_option(SettingsManager.SHARPENING, val)
	viewport.set_sharpen_intensity(val / 100.0)
	

func _msaa_item_selected(var index: int) -> void:
	var item_id: int = msaa_options.get_item_id(index)
	var samples: int = item_id * 2
	
	settings_manager.set_option(SettingsManager.MSAA_LEVEL, samples)
	viewport.set_msaa(samples)
