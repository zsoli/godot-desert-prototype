extends Control

export var next_scene: String

export var start_btn_node: NodePath
var start_btn: Button

export var progress_bar_node: NodePath
var progress_bar: ProgressBar

export var anim_node: NodePath
var animation_player: AnimationPlayer

var progress: float = 0
var pba: float = 0.0
var scene_switcher: SceneSwitcher = GlobalVariables.scene_switcher


func _ready() -> void:
	var ret: int

	start_btn = get_node(start_btn_node)
	assert(start_btn != null)

	progress_bar = get_node(progress_bar_node)
	assert(progress_bar != null)


	animation_player = get_node(anim_node)
	assert(animation_player != null)

	ret = animation_player.connect("animation_finished", self, "_anim_finished")
	assert(ret == OK)

	ret = start_btn.connect("pressed", self, "_on_start_btn_pressed")
	assert(ret == OK)

	progress_bar.modulate.a = 0.0


func _on_loading(var _progress: int):
	self.progress = _progress
	if (progress == 100): pba = 0.0


func _anim_finished(anim: String):
	if anim != "EaseOut":
		return

	print_debug("fade finished")
	scene_switcher.goto_scene(next_scene)


func _on_start_btn_pressed() -> void:
	start_btn.set_disabled(true)
	animation_player.play("EaseOut")
	pba = 1.0

	var ret := scene_switcher.load_scene(get_parent(), next_scene).connect("loading", self, "_on_loading")
	assert(ret == OK)


func _process(_delta):
	progress_bar.value = lerp(progress_bar.value, progress, 0.5)
	progress_bar.modulate.a = lerp(progress_bar.modulate.a, pba, 0.05)
	# print_debug("progressbar val: %f" % (progress_bar.value - progress))
