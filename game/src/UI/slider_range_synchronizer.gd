class_name SliderRangeSynchronizer
extends HBoxContainer

signal value_changed(value)

# slider
export var slider_node: NodePath
var slider: Slider

# range
export var range_node: NodePath
var _range: Range


func _ready() -> void:
	var ret: int

	slider = get_node(slider_node)
	assert(slider != null)
	ret = slider.connect("value_changed", self, "_slider_value_changed")
	assert(ret == OK)

	_range = get_node(range_node)
	assert(_range != null)
	ret = _range.connect("value_changed", self, "_range_value_changed")
	assert(ret == OK)


func _slider_value_changed(val: float) -> void:
	_range.set_value(val)
	self._emit_signal(val)


func _range_value_changed(val: float) -> void:
	slider.set_value(val)
	self._emit_signal(val)


func _emit_signal(var val: float) -> void:
	emit_signal("value_changed", val)
	

func set_value(var value: float) -> void:
	slider.set_value(value)
	_range.set_value(value)
