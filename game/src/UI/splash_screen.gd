extends MarginContainer

export var animation_node: NodePath
var animation

export var progress_bar_node: NodePath
var progress_bar: ProgressBar

export var next_scene: String

var pba: float = 1.0
var progress: int = 0
var scene_switcher := GlobalVariables.scene_switcher


func _ready():
	var ret: int

	animation = get_node(animation_node)
	assert(animation != null)

	progress_bar = get_node(progress_bar_node)
	assert(progress_bar != null)

	assert(next_scene != null && next_scene != "")

	ret = animation.connect("animation_finished", self, "_anim_finished")
	assert(ret == OK)

	ret = scene_switcher.load_scene(self, next_scene).connect("loading", self, "_on_loading")
	assert(ret == OK)

	progress_bar.modulate.a = 0.0


func _anim_finished(anim: String):
	if anim != "FadeInOut":
		return

	print_debug("fade finished")
	scene_switcher.goto_scene(next_scene)


func _on_loading(var _progress: int):
	self.progress = _progress
	if (progress == 100): pba = 0.0
	

func _process(_delta):
	progress_bar.value = lerp(progress_bar.value, progress, 0.5)
	progress_bar.modulate.a = lerp(progress_bar.modulate.a, pba, 0.05)
