extends Control

export var label1_node: NodePath
var label1: RichTextLabel

export var label2_node: NodePath
var label2: RichTextLabel

export var label3_node: NodePath
var label3: RichTextLabel

export var player_node: NodePath
var player: Player

export var desert_controller_node: NodePath
var desert_controller: DesertController

export var game_manager_node: NodePath
var game_manager: GameManager


func _ready() -> void:
	label1 = get_node(label1_node)
	assert(label1 != null)

	label2 = get_node(label2_node)
	assert(label2 != null)

	if !player_node.is_empty():
		player = get_node(player_node)
	if !desert_controller_node.is_empty():
		desert_controller = get_node(desert_controller_node)
	if !game_manager_node.is_empty():
		game_manager = get_node(game_manager_node)


func _process(delta) -> void:
	self.visible = GlobalVariables.debug_mode
	if !self.visible:
		return

	self._update_label1(delta)
	self._update_label2()
	self._update_label3()


func _update_label1(var delta: float) -> void:
	var fps: float = Engine.get_frames_per_second()
	var frame_time: float = delta * 1000.0

	label1.bbcode_text = """[code][table=1]
		[cell]fps: {}[/cell]
		[cell]frame_time: {} ms[/cell]
		[/table][/code]
	""".format([
		fps, 
		"%3.1f" % frame_time, 
	], "{}")


func _update_label2() -> void:
	if game_manager == null || desert_controller == null:
		return

	label2.bbcode_text = """[code][table=1]
		[cell]storm_factor: {}[/cell]
		[cell]current_starting_distance: {}[/cell]
		[/table][/code]
	""".format([
		"%0.2f" % desert_controller.storm_factor,
		"%0.2f" % game_manager.current_closest_max
	], "{}")


func _update_label3() -> void:
	if player == null:
		return
	
	label1.bbcode_text = """[code][table=1]
		[cell]player_velocity: {}, {}, {}[/cell]
		[cell]camera_fov: {}[/cell]
		[cell]player_on_floor: {}[/cell]
		[cell]floor_angle: {} deg[/cell]
		[cell]active_item: {}[/cell]
		[cell]highlighted_item: {}[/cell]
		[cell]fall_time: {}[/cell]
		[/table][/code]
	""".format([
		"%5.1f" % player.velocity.x, 
		"%5.1f" % player.velocity.y,
		"%5.1f" % player.velocity.z,
		"%0.0f" % player.camera.fov,
		player.is_on_floor,
		"%4.1f" % rad2deg(player.get_floor_angle()),
		player.item_picked_up,
		player.item_highlighted,
		"%0.2f" % player.fall_t
	], "{}")
