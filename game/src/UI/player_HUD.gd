extends Control

export var label_node: NodePath
var label: Label


func show_interact() -> void:
	label.set_visible(true)


func hide_interact() -> void:
	label.set_visible(false)


func _ready():
	label = get_node(label_node)
	assert(label != null)

	label.set_visible(false)
