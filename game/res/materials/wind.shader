shader_type spatial;
render_mode depth_draw_alpha_prepass, cull_disabled, world_vertex_coords;

uniform sampler2D texture_albedo : hint_albedo;
uniform vec4 transmission : hint_color;
uniform vec4 color: hint_color;

uniform float sway_speed = 1.0;
uniform float sway_strength = 0.05;
uniform float sway_phase_len = 8.0;

void fragment() {
    vec4 albedo_tex = texture(texture_albedo, UV);
    ALBEDO = albedo_tex.rgb * color.rgb;
    ALPHA = albedo_tex.a * color.a;
    METALLIC = 0.0;
    ROUGHNESS = 1.0;
    TRANSMISSION = transmission.rgb;
}

void vertex() {
	float strength = COLOR.r * sway_strength;
	VERTEX.x += sin(VERTEX.x * sway_phase_len * 1.0 + TIME * sway_speed * 1.0) * strength;
	VERTEX.y += sin(VERTEX.y * sway_phase_len * 1.1 + TIME * sway_speed * 1.1) * strength;
	VERTEX.z += sin(VERTEX.z * sway_phase_len * 1.2 + TIME * sway_speed * 1.2) * strength;
}