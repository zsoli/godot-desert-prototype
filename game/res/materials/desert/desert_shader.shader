shader_type spatial;

/*
NOTES:
	texture mask white -> texture 2
	texture mask black -> texture 1
	wind only applied to texture 2
*/

/* texture 1 (simple noise) */
uniform sampler2D texture1: hint_albedo;
uniform float texture1_scale: hint_range(0, 1000) = 10.0;
uniform vec4 texture1_color: hint_color;

/* texture 2 (wavy) */
uniform sampler2D texture2: hint_albedo;
uniform float texture2_scale: hint_range(0, 1000) = 10.0;
uniform vec4 texture2_color: hint_color;

/* texture mask */
uniform sampler2D texture_mask;
uniform float texture_mask_scale: hint_range(0, 100) = 10.0;
uniform float mask_strength: hint_range(1, 5, 1) = 2.0;

/* wind 1 (large) */
uniform sampler2D wind_texture: hint_black;
uniform float wind_scale: hint_range(0, 100) = 1.0;
uniform float wind_speed: hint_range(0, 100) = 0.1;
uniform vec4 wind_color: hint_color;

/* wind 2 (detail) */
uniform sampler2D wind_detail_texture: hint_black;
uniform float wind_detail_scale: hint_range(0, 100) = 1.0;
uniform float wind_detail_speed: hint_range(0, 100) = 0.1;

/* wind effects */
uniform float wind_strength: hint_range(0, 10) = 0.05;
uniform float wind_direction: hint_range(0, 360) = 0.0;

uniform float detail_mix_mode: hint_range(-1, 1, 2) = 1;
uniform float wind_mix_mode: hint_range(-1, 1, 2) = 1;

uniform float wind_offset = 0.0;


void fragment() {
	/* process the albedo textures */
	vec3 tex[2];
	tex[0] = texture(texture1, UV * texture1_scale).rgb;
	tex[1] = texture(texture2, UV * texture2_scale).rgb;

	vec3 tex_mask = texture(texture_mask, UV * texture_mask_scale).rgb;
	vec3 tex_detail = texture(wind_detail_texture, UV * (texture1_scale + texture2_scale) / 40.0 / 2f).rgb;
	
	vec3 tex_colorized[2];
	tex_colorized[0] = mix(tex[0], tex[0] * texture1_color.rgb, texture1_color.a);
	tex_colorized[1] = mix(tex[1], tex[1] * texture2_color.rgb, texture2_color.a);

	vec3 tex_mix_weight = pow(tex_mask, vec3(mask_strength));
	vec3 tex_fin = mix(tex_colorized[0], tex_colorized[1], tex_mix_weight);
	tex_fin = mix(tex_fin, tex_fin + tex_detail * detail_mix_mode, tex_detail.r * 0.2);

	/* calculate wind textures */
	float wind_rot_rad = radians(wind_direction);
	vec2 wind_vel = vec2(sin(wind_rot_rad), cos(wind_rot_rad)) * wind_offset;

	vec2 wind_uv[2];
	wind_uv[0] += (UV + wind_vel) * wind_scale;
	wind_uv[1] += (UV + wind_vel) * wind_detail_scale;

	vec3 wind[2];
	wind[0] = texture(wind_texture, wind_uv[0]).rgb;
	wind[1] = texture(wind_detail_texture, wind_uv[1]).rgb;

	vec3 wind_mix_weight = tex_mix_weight;
	vec3 wind_fin = mix(vec3(0), wind_color.rgb * wind_color.a - (wind[0] + wind[1]), wind_mix_weight);
	
	/* color result (mix wind with albedo textures) */
	ALBEDO = mix(tex_fin, tex_fin + wind_fin.rgb * wind_mix_mode, wind_fin.r * wind_strength);
	//ALPHA = alpha;
	SPECULAR = 0.0; // default is 0.5
	ROUGHNESS = 1.0;
	METALLIC = 0.0;
}