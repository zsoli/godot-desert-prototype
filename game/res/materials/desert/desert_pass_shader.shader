shader_type spatial;

/*
NOTES:
	texture mask white -> texture 2
	texture mask black -> texture 1
	wind only applied to texture 2
*/

uniform float alpha: hint_range(0, 1) = 1.0;

/* texture mask */
uniform sampler2D texture_mask;
uniform float texture_mask_scale: hint_range(0, 100) = 10.0;
uniform float mask_strength: hint_range(1, 5, 1) = 2.0;

/* wind 1 (large) */
uniform sampler2D wind_texture: hint_black;
uniform float wind_scale: hint_range(0, 100) = 1.0;
uniform vec4 wind_color: hint_color;

/* wind 2 (detail) */
uniform sampler2D wind_detail_texture: hint_black;
uniform float wind_detail_scale: hint_range(0, 100) = 1.0;

/* wind effects */
uniform float wind_strength: hint_range(0, 10) = 0.05;

void fragment() {
	vec3 tex_mask = texture(texture_mask, UV * texture_mask_scale).rgb;
	vec3 tex_mix_weight = pow(tex_mask, vec3(mask_strength));

	vec2 wind_uv[2];
	wind_uv[0] += (UV) * wind_scale;
	wind_uv[1] += (UV) * wind_detail_scale;
	
	vec3 wind[2];
	wind[0] = texture(wind_texture, wind_uv[0]).rgb;
	wind[1] = texture(wind_detail_texture, wind_uv[1]).rgb;

	vec3 wind_mix_weight = tex_mix_weight;
	vec3 wind_fin = mix(vec3(0), wind[0] + wind[1], wind_mix_weight);
	wind_fin *= wind_color.rgb;
	
	ALBEDO = mix(vec3(0), wind_fin.rgb, wind_fin.r * wind_strength);
	ALPHA = wind_fin.r * alpha;
	SPECULAR = 0.0; // default is 0.5
	ROUGHNESS = 1.0;
	METALLIC = 0.0;
}