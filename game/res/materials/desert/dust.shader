shader_type spatial;
render_mode cull_disabled, unshaded;

uniform sampler2D dust_texture;
uniform vec4 albedo_color: hint_color;

void fragment() {
	vec3 dust_color = texture(dust_texture, UV).rgb;
	
	ALBEDO = albedo_color.rgb * dust_color;
	ALPHA = albedo_color.r / 2f;
}