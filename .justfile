set dotenv-load := true

# constants
LINUX_DEBUG_TEMPLATE := "linux_x11_64_debug"
LINUX_RELEASE_TEMPLATE := "linux_x11_64_release"
WIN_DEBUG_TEMPLATE := "windows_64_debug.exe"
WIN_RELEASE_TEMPLATE := "windows_64_release.exe"
RED := '\033[0;31m'
NC := '\033[0m'
BOLD := '\033[1m'
REGULAR := '\033[0m'

# variables
TOPDIR := justfile_directory()
ENGINE_VERSION := "$(just _get-engine-version)"

ENGINE_EXECUTABLE := "godot." + ENGINE_VERSION
HEADLESS_ENGINE_EXECUTABLE := "godot.headless." + ENGINE_VERSION

GODOT := ENGINE_BINARIES_DIR + '/' + ENGINE_EXECUTABLE + " --path " + PROJ_DIR
GODOT_HEADLESS := ENGINE_BINARIES_DIR + '/' + HEADLESS_ENGINE_EXECUTABLE + " --path " + PROJ_DIR

PROJ_DIR := TOPDIR + "/${GODOT_PROJECT_DIR}"
LOG_DIR := TOPDIR + "/logs"
BUILD_DIR := TOPDIR + "/build"
ENGINE_BINARIES_DIR := TOPDIR + "/engine-binaries/" + ENGINE_VERSION
TEMPLATE_DIR := ENGINE_BINARIES_DIR + "/editor_data/templates/" + ENGINE_VERSION

BUILD_NAME := "build." + ENGINE_VERSION
ENGINE_LOG_FILE := "engine." + ENGINE_VERSION + ".log"
GAME_LOG_FILE := "game." + ENGINE_VERSION + ".log"

# aliases
alias r := run
alias e := editor
alias ea := export-all
alias eld := export-linux-debug
alias elr := export-linux-release
alias ewd := export-windows-debug
alias ewr := export-windows-release


# Select recipe with fzf fuzzy chooser if no recipe is specified.
_default:
	@just --choose

	
##################
# USER FUNCTIONS #
##################


# Opens the Godot editor.
editor: compile-engine
	#!/usr/bin/env bash
	set -euo pipefail

	stdbuf -o 0 {{GODOT}} --editor > "{{LOG_DIR}}/{{ENGINE_LOG_FILE}}" 2>&1 &


# Run a specific scene (without .tscn).
run SCENE *OPTIONAL_ARGS: _dirs compile-engine
	#!/usr/bin/env bash
	set -euo pipefail

	declare -r extra_args="--scene {{SCENE}}.tscn --debug {{OPTIONAL_ARGS}}"
	stdbuf -o 0 {{GODOT}} ${extra_args} |& tee "{{LOG_DIR}}/{{GAME_LOG_FILE}}"


# TODO: Not quite working yet.
# Remote debug a specific scene (without .tscn).
remote-debug SCENE *OPTIONAL_ARGS: _dirs compile-engine
	#!/usr/bin/env bash
	set -euo pipefail

	declare -r extra_args="--scene {{SCENE}}.tscn --remote-debug 127.0.0.1:6007 {{OPTIONAL_ARGS}}"
	stdbuf -o 0 {{GODOT}} ${extra_args} |& tee '{{LOG_DIR}}/{{ENGINE_LOG_FILE}}'

	echo "started"


# Export Linux release version.
export-linux-release: compile-engine-headless build-export-template-linux-release
	#!/usr/bin/env bash
	set -euo pipefail

	just _log_info "Exporting Linux release..."
	{{GODOT_HEADLESS}} --export "Linux/X11" "{{BUILD_DIR}}/{{BUILD_NAME}}-release" || true
	just _log_info "Exporting finished"


# Export Linux debug version.
export-linux-debug: compile-engine-headless build-export-template-linux-debug
	#!/usr/bin/env bash
	set -euo pipefail

	just _log_info "Exporting for Linux debug..."
	{{GODOT_HEADLESS}} --export-debug "Linux/X11" "{{BUILD_DIR}}/{{BUILD_NAME}}-debug" || true
	just _log_info "Exporting finished"


# Export Windows release version.
export-windows-release: compile-engine-headless build-export-template-windows-release
	#!/usr/bin/env bash
	set -euo pipefail

	just _log_info "Exporting for Windows release..."
	{{GODOT_HEADLESS}} --export "Windows Desktop" "{{BUILD_DIR}}/{{BUILD_NAME}}-release.exe" || true
	just _log_info "Exporting finished"


# Export Windows debug version.
export-windows-debug: compile-engine-headless build-export-template-windows-debug
	#!/usr/bin/env bash
	set -euo pipefail

	just _log_info "Exporting for Windows debug..."
	{{GODOT_HEADLESS}} --export-debug "Windows Desktop" "{{BUILD_DIR}}/{{BUILD_NAME}}-debug.exe" || true
	just _log_info "Exporting finished"


# Build godot editor version.
compile-engine:
	#!/usr/bin/env bash
	set -euo pipefail

	just _build-engine-generic "{{ENGINE_EXECUTABLE}}" "platform=linuxbsd target=release_debug"


# Build headless version of engine for exporting.
compile-engine-headless:
	#!/usr/bin/env bash
	set -euo pipefail

	just _build-engine-generic "{{HEADLESS_ENGINE_EXECUTABLE}}" "platform=server target=release_debug"


# Build Linux release export template.
build-export-template-linux-release:
	#!/usr/bin/env bash
	set -euo pipefail

	just _build-export-template-generic "{{LINUX_RELEASE_TEMPLATE}}" "${LINUX_RELEASE_TEMPLATE_BUILD_CONF}"
	strip "{{TEMPLATE_DIR}}/{{LINUX_RELEASE_TEMPLATE}}"


# Build Linux debug export template.
build-export-template-linux-debug:
	#!/usr/bin/env bash
	set -euo pipefail

	just _build-export-template-generic "{{LINUX_DEBUG_TEMPLATE}}" "${LINUX_DEBUG_TEMPLATE_BUILD_CONF}"


# Build Windows release export template.
build-export-template-windows-release:
	#!/usr/bin/env bash
	set -euo pipefail

	just _build-export-template-generic "{{WIN_RELEASE_TEMPLATE}}" "${WIN_RELEASE_TEMPLATE_BUILD_CONF}"
	strip "{{TEMPLATE_DIR}}/{{WIN_RELEASE_TEMPLATE}}"


# Build Windows debug export template.
build-export-template-windows-debug:
	#!/usr/bin/env bash
	set -euo pipefail

	just _build-export-template-generic "{{WIN_DEBUG_TEMPLATE}}" "${WIN_DEBUG_TEMPLATE_BUILD_CONF}"


# Clean build cache files.
clean-build-cache:
	#!/usr/bin/env bash
	set -euo pipefail

	rm -rvf "{{TOPDIR}}/${ENGINE_DIR}/__pycache__"
	rm -rvf "{{TOPDIR}}/${ENGINE_DIR}/.sconf_temp"


# Clean game cache files.
clean-game-cache:
	#!/usr/bin/env bash
	set -euo pipefail

	rm -rvf "{{PROJ_DIR}}/.godot"
	rm -rvf "{{PROJ_DIR}}/.import"


# Clean export templates.
clean-engine-binaries:
	#!/usr/bin/env bash
	set -euo pipefail

	rm -vf "{{TEMPLATE_DIR}}/{{LINUX_DEBUG_TEMPLATE}}"
	rm -vf "{{TEMPLATE_DIR}}/{{LINUX_RELEASE_TEMPLATE}}"
	rm -vf "{{TEMPLATE_DIR}}/{{WIN_DEBUG_TEMPLATE}}"
	rm -vf "{{TEMPLATE_DIR}}/{{WIN_RELEASE_TEMPLATE}}"
	rm -vf "{{ENGINE_BINARIES_DIR}}/{{ENGINE_EXECUTABLE}}"
	rm -vf "{{ENGINE_BINARIES_DIR}}/{{HEADLESS_ENGINE_EXECUTABLE}}"


# Clean all generated files.
clean-all: clean-build-cache clean-engine-binaries clean-game-cache


# Export both debug and release Linux versions.
export-linux-all: export-linux-debug export-linux-release


# Export both debug and release Linux versions.
export-windows-all: export-windows-debug export-windows-release


# Export both Linux and Windows versions.
export-all: export-linux-all export-windows-all


######################
# INTERNAL FUNCTIONS #
######################


# Helper function for building engine executables.
_compile-engine TARGET BUILD_CONFIG: _dirs
	#!/usr/bin/env bash
	set -euo pipefail

	cd "{{TOPDIR}}/${ENGINE_DIR}" || { just _log_err "Failed to enter '{{TOPDIR}}/${ENGINE}' directory!" && exit; }
	
	if [[ ! -f "{{TARGET}}" ]]; then
		just _log_info "Building '{{TARGET}}'..."
		scons -j$(nproc) {{BUILD_CONFIG}}

		# move the latest file by access time
		cd "bin" || { just _log_err "Failed to enter '$(pwd)/bin' directory!" && exit; }
		mv "$(ls -t1 | head -n 1)" "{{TARGET}}"
		
		just _log_info "Finished building '{{TARGET}}'"
	else
		just _log_info "Binary '{{TARGET}}' is already built"
	fi


# Get engine version from version.py in 'major.minor.patch.status' format.
_get-engine-version:
	#!/usr/bin/env bash
	set -euo pipefail

	declare -r file_content="$(<${ENGINE_DIR}/version.py)"
	declare -ri major="$(awk -F '[[:space:]]' '/^major/ {print $NF}' <<< ${file_content})"
	declare -ri minor="$(awk -F '[[:space:]]' '/^minor/ {print $NF}' <<< ${file_content})"
	declare -ri patch="$(awk -F '[[:space:]]' '/^patch/ {print $NF}' <<< ${file_content})"
	declare -r status="$(awk -F '[[:space:]]' '/^status/ {print $NF}' <<< ${file_content})"
	
	echo "${major}.${minor}.${patch}.${status:1:-1}"


# Helper function for building templates.
_build-export-template-generic TEMPLATE TEMPLATE_CONF:
	#!/usr/bin/env bash
	set -euo pipefail

	just _compile-engine "{{TEMPLATE_DIR}}/{{TEMPLATE}}" "{{TEMPLATE_CONF}}"


# Helper function for building engine binaries.
_build-engine-generic BINARY BINARY_CONF:
	#!/usr/bin/env bash
	set -euo pipefail

	just _compile-engine "{{ENGINE_BINARIES_DIR}}/{{BINARY}}" "{{BINARY_CONF}}"


# Helper function to make sure directories exist.
_dirs:
	#!/usr/bin/env bash
	set -euo pipefail

	mkdir -p "{{BUILD_DIR}}"
	mkdir -p "{{ENGINE_BINARIES_DIR}}"
	mkdir -p "{{LOG_DIR}}"
	mkdir -p "{{TEMPLATE_DIR}}"

	# To enable self-contained mode.
	# The engine will store the user data in ENGINE_BINARIES_DIR/editor_data directory.
	touch "{{ENGINE_BINARIES_DIR}}/._sc_"


# Logging function. Info level.
_log_info STR:
    @echo -e "{{BOLD}}>> {{STR}}{{REGULAR}}"


# Logging function. Error level.
_log_err STR:
    @echo -e "{{RED}}{{BOLD}}>> {{STR}}{{REGULAR}}{{NC}}"